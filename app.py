from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return "Hello, welcome to python with Docker! I'm glad you came!"
