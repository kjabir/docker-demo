# Hello-World-Docker-Demo
Basic Docker demo for deploying, running and making changes to container images, and orchestrating containers with k8s.


## How to create, edit, deploy a container image

### Prerequisites: 
	DockerHub account
	Docker desktop installed on local machine
	- installation includes CLI tools, BuildKit, Docker Engine and Docker Compose
	Git - Github or other git tools 

1. Clone docker-demo repository and open on your local machine

2. Review files
    - dockerfile: Our docker image build script which tells docker what to package within the image
    - app.py: The python executable file for running our app
    - requirements.txt: Lists all the libraries to install for the python app
    - Pipfile: ensures the correct version of dependencies in requirements.txt, and their required dependencies are installed
3. Build the Docker image
    - Run `docker build --tag [appName]:[tag]] .` 
    - This command builds the image and stores it in your local docker repository, with the name:tag of the image and path to where the image should be saved (`.` indicates the default location for your docker installation). 
4. Inspect
    - Run `docker images` to list the images in your docker repos
        - Repository, Tag, Image ID, Created, Size
    - Run `docker inspect [tag/id]` to inspect an image
5. Run the image
    - Run `docker run [imageName]`
        - This command runs the docker image on Docker Engine
            - Containerd: K8s Container Runtime Interface (CRI)
            - BuildKit: Uses Dockerfile instructions to build the Docker image
            - Docker CLI: Command line interface for working with docker images and containers
        - If no tag is specified, docker runs the image with the tag `:latest`
            -   To specify a tag, append the tag name to the imaga name `[imageName]:[tag]`
6. Docker gotcha
    - navigate to the provided URL
        - You'll see the page is not displaying the 'Hello World!' message.
        - If you wait long enough you see a request timeout error
        - URL is the internal URL for the container, which is bound to `localhost:[port]`
    - Open a new terminal window and run curl localhost:5000
        - No response or connection refuesed
7. Publish port
    -   `--publish` (`-p`) flag: format `[host port]:[container port]`
        - This flag binds the container's port to the host's exposed port, making the container port accessible on the localhost
    -   Stop the running container
    - Run `docker run -p 8000:5000 [image]:[tag]`
        - curl localhost:8080
        - Access localhost:8000 from browser
    - To run the container in detached mode you can use the `--detach` (`-d`) flag `docker run [-dp]/[-d -p] 8000:5000 [image]:[tag]`
        - In detached mode, Docker prints the Container ID, and runs the container in the background
        - To stop a detached container, run `docker stop [container id]`
8. Making a change to the image
    - Run `docker images` to see the list of available images
    - Change 'Hello World!' message and save
    - Run `docker build --tag [appName]:[tag]] .` with a different tag from before
        - Useful for comparing images, testing changes to images
        - Using the same tag will overwrite the existing image:tag
            - You can still access overwritten images by specifying Image ID `docker run -dp 8000:5000 [Image ID]`
    - Run `docker images` to see the list of available images    
9. Run new image
    - Run `docker run -dp 8000:5000 [image]:[tag]` using the new image you created
        - curl localhost:8080
        - Access localhost:8000 from browser
10. Managing images
    - Run `docker images` or `docker image ls` to see the list of available images
    -   Command	                Description
        `docker image build`	Build an image from a Dockerfile
        `docker image history`	Show the history of an image
        `docker image import`	Import the contents from a tarball to create a filesystem image
        `docker image inspect`	Display detailed information on one or more images
        `docker image load`	    Load an image from a tar archive or STDIN
        `docker image ls`	    List images
        `docker image prune`	Remove unused images
        `docker image pull`	    Pull an image or a repository from a registry
        `docker image push`	    Push an image or a repository to a registry
        `docker image rm`	    Remove one or more images (may have to use the `--force` (`-f`) flag to force the deletion)
        `docker image save`	    Save one or more images to a tar archive (streamed to STDOUT by default)
        `docker image tag`	    Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE
## Docker Compose


## Orchestrating containers with Kubernetes (K8s)
